<?php

namespace Doz;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    protected $table = 'slides';

    protected $fillable = [
        'heading_first',
        'heading_second',
        'paragraph',
        'text_button',
        'url',
        'photo'
    ];

    protected $guarded = 'id';
}
