<?php

namespace Doz;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $table = 'stores';

    protected $fillable = [
        'store'
    ];

    protected $guarded = 'id';
}
