<?php

namespace Doz;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';

    protected $fillable = [
        'titulo',
        'slug',
        'borrador',
        'cuerpo'
    ];

    protected $guarded = 'id';
}
