<?php

namespace Doz;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    protected $fillable = [
        'name', 'description', 'price', 'slug', 'categories_id', 'users_id'
    ];
}
