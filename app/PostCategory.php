<?php

namespace Doz;

use Illuminate\Database\Eloquent\Model;

class PostCategory extends Model
{
    protected $table = 'post_categories';

    protected $fillable = [
        'category',
        'subcategory'
    ];

    protected $guarded = 'id';
}
