<?php

namespace Doz;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    protected $table = 'product_categories';

    protected $fillable = [
        'category',
        'subcategory',
        'slug'
    ];

    protected $guarded = 'id';

    public function parent()
    {
        return $this->belongsTo('Doz\ProductCategory', 'subcategory');
    }

    public function children()
    {
        return $this->hasMany('Doz\ProductCategory', 'subcategory');
    }
}
