<?php

namespace Doz\Http\Controllers;

use Doz\User;
use Illuminate\Http\Request;

use Doz\Http\Requests;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $users = User::paginate(10);

        return view('app.users.index', compact('users'));
    }
}
