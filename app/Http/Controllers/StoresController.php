<?php

namespace Doz\Http\Controllers;

use Doz\Store;
use Illuminate\Http\Request;

use Doz\Http\Requests;

class StoresController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $stores = Store::paginate(10);

        return view('app.stores.index', compact('stores'));
    }
}
