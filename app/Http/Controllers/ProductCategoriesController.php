<?php

namespace Doz\Http\Controllers;

use Doz\Http\Requests\ProductCategoryRequest;
use Doz\ProductCategory;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;

class ProductCategoriesController extends Controller
{
    public function index()
    {
        $categories = ProductCategory::orderBy('created_at', 'ASC')->paginate(10);

        return view('app.products.categories.index', compact('categories'));
    }

    public function create()
    {
        $categories = ProductCategory::all()->pluck('category', 'id')
            ->prepend("Categoria padre", 0);

        return view('app.products.categories.create', compact('categories'));
    }

    public function store(ProductCategoryRequest $request)
    {
        $category = new ProductCategory();

        $category->fill($request->all());

        try {

            $category->save();

            Flash::success('Categoria creada correctamente');

        } catch (Exception $e) {

            Flash::error('Error ' . $e . ' al crear la categoria');

        }

        return redirect(route('categories.create'));
    }

    public function edit($id)
    {
        $category = ProductCategory::find($id);

        $categories = ProductCategory::all()->pluck('category', 'id')
            ->prepend("Categoria padre", 0);

        return view('app.products.categories.edit', compact('category', 'categories'));
    }

    public function update(ProductCategoryRequest $request, $id)
    {
        $category = ProductCategory::find($id);

        $category->fill($request->all());

        try {

            $category->save();

            Flash::success('Categoria modificada correctamente.');

        } catch (Exception $e) {

            Flash::error('Error ' . $e . ' al modificar la categoria.');
        }

        return redirect(route('categories.edit', $category->id));
    }

    public function delete($id)
    {

    }
}
