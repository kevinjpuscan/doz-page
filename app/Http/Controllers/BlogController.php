<?php

namespace Doz\Http\Controllers;

use Doz\Post;
use Doz\ProductCategory;
use Illuminate\Http\Request;

use Doz\Http\Requests;

class BlogController extends Controller
{
    public function index()
    {
        $categories = ProductCategory::where('subcategory', 0)->with('children')->get();

        $brands = ProductCategory::where('subcategory', '<>', 0)->get();

        return view('blog', compact('categories', 'brands'));
    }

    public function show($slug)
    {
        $post = Post::where('slug', $slug)->firstOrFail();

        $categories = ProductCategory::where('subcategory', 0)->with('children')->get();

        $brands = ProductCategory::where('subcategory', '<>', 0)->get();

        return view('post', compact('post', 'categories', 'brands'));
    }
}
