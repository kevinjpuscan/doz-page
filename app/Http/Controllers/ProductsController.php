<?php

namespace Doz\Http\Controllers;

use Doz\ProductCategory;
use Doz\Product;
use Doz\Http\Requests\ProductRequest;
use Doz\Http\Requests\ProductCategoryRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laracasts\Flash\Flash;
use League\Flysystem\Exception;

class ProductsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $products = Product::orderBy('created_at', 'ASC')->paginate(10);

        return view('app.products.index', compact('products'));
    }

    public function create()
    {
        $categories = ProductCategory::where('subcategory', '<>', 0)->pluck('category', 'id');

        return view('app.products.create', compact('categories'));
    }

    public function store(ProductRequest $request)
    {
        $product = new Product();

        $product->fill($request->all());

        $product->users_id = Auth::user()->id;

        if($request->hasFile('photo')) {
            $path = public_path('/product-images');

            $image = $request->file('photo');

            $imageName = time() . $image->getClientOriginalExtension();

            try {

                $product->photo = "/product-images/" . $imageName;

                $image->move($path, $imageName);

                $product->save();

                Flash::success('Producto creado correctamente.');

            } catch(Exception $e) {

                Flash::error('Error ' . $e . ' al crear el producto.');
            }
        }

        return redirect(route('products.create'));
    }

    public function edit($id)
    {
        $product = Product::find($id);

        $categories = ProductCategory::where('subcategory', '<>', 0)->pluck('category', 'id');

        return view('app.products.edit', compact('product', 'categories'));
    }

    public function update(ProductRequest $request, $id)
    {
        $product = Product::find($id);

        $product->fill($request->all());

        $product->users_id = Auth::user()->id;

        if($request->hasFile('photo')) {

            $path = public_path('/product-images');

            $image = $request->file('photo');

            $imageName = time() . $image->getClientOriginalExtension();

            $product->photo = "/product-images/" . $imageName;

            $image->move($path, $imageName);

            try {
                $product->save();

                Flash::message('Producto modificado correctamente.');

            } catch(Exception $e) {

                Flash::error('Error ' . $e . ' al modificar el producto.');
            }
        } else {

            try {
                $product->save();

                Flash::message('Producto modificado correctamente.');

            } catch(Exception $e) {

                Flash::error('Error ' . $e . ' al modificar el producto.');
            }
        }

        return redirect(route('products.edit', $product->id));
    }

    public function delete($id)
    {

    }

    public function indexCategory()
    {
        $categories = ProductCategory::paginate(10);

        return view('app.products.categories.index', compact('categories'));
    }

    public function createCategory()
    {
        return view('app.products.categories.create');
    }

    public function storeCategory(ProductCategoryRequest $request)
    {
        $category = new ProductCategory();

        $category->fill($request->all());

        try {
            $category->save();

            Flash::success('Categoria creada correctamente.');

        } catch (Exception $e) {

            Flash::error('Error ' . $e . ' al crear la categoria');
        }

        return redirect(route('products.caregory.create'));
    }

    public function editCategory($id)
    {
        $category = ProductCategory::find($id);

        return view('app.products.categories.edit', compact('category'));
    }

    public function updateCategory(ProductCategoryRequest $request, $id)
    {
        $category = ProductCategory::find($id);

        $category->fill($request->all());

        try {
            $category->save();

            Flash::success('Categoria modificada correctamente.');

        } catch (Exception $e) {

            Flash::error('Error ' . $e . ' al modificar la categoria.');
        }

        return redirect(route('products.edit', $category->id));

    }

    public function deleteCategory($id)
    {

    }
}
