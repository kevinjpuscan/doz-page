<?php

namespace Doz\Http\Controllers;

use Doz\Product;
use Doz\ProductCategory;
use Illuminate\Http\Request;

use Doz\Http\Requests;

class WebController extends Controller
{
    public function index()
    {
        $slides = \Doz\Slide::all();

        $featured_products = \Doz\Product::paginate(6);

        $recommended_products = \Doz\Product::paginate(6);

        $categories = ProductCategory::where('subcategory', 0)->with('children')->get();

        $brands = ProductCategory::where('subcategory', '<>', 0)->get();

        return view('web', compact('slides', 'featured_products', 'recommended_products', 'categories', 'brands'));
    }

    public function contact()
    {
        return view('contact');
    }

    public function show($slug)
    {
        $product = Product::where('slug', $slug)->first();

        $categories = ProductCategory::where('subcategory', 0)->with('children')->get();

        $brands = ProductCategory::where('subcategory', '<>', 0)->get();

        return view('product-detail', compact('product', 'categories', 'brands'));
    }
}
