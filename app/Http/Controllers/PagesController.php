<?php

namespace Doz\Http\Controllers;

use Doz\Page;
use Illuminate\Http\Request;

use Doz\Http\Requests;

class PagesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $pages = Page::paginate(10);

        return view('app.pages.index', compact('pages'));
    }
}
