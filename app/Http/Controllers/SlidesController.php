<?php

namespace Doz\Http\Controllers;

use Doz\Slide;
use Doz\Http\Requests\SlideRequest;
use Laracasts\Flash\Flash;

class SlidesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $slides = Slide::paginate(5);

        return view('app.slides.index', compact('slides'));
    }

    public function create()
    {
        return view('app.slides.create');
    }

    public function store(SlideRequest $request)
    {
        $slide = new Slide();

        $slide->fill($request->all());

        $destino = 'banners';

        $imagen = $request->file('photo');

        if($request->hasFile('photo'))
        {
            $archivo = '/' . $destino . '/' . $imagen->getClientOriginalName();

            $imagen->move($destino, $archivo);

            $slide->photo = $archivo;
        } else {

            $slide->photo = '/bower_components/eshopper/images/banner-1.png';
        }

        if($slide->save()){

            Flash::message('Banner creado correctamente.');
        }

        return redirect(route('slides.index'));
    }

    public function edit($id)
    {
        $slide = Slide::find($id);

        return view('app.slides.edit', compact('slide'));
    }

    public function update(SlideRequest $request, $id)
    {
        $slide = Slide::find($id);

        $slide->fill($request->all());

        $destino = 'banners';

        $imagen = $request->file('imagen');

        if($request->hasFile('imagen'))
        {
            $archivo = '/' . $destino . '/' . $imagen->getClientOriginalName();

            $imagen->move($destino, $archivo);

            $slide->imagen = $archivo;
        }

        if($slide->save()){

            Flash::message('Banner modificado correctamente.');
        }

        return redirect(route('slides.index'));
    }

    public function destroy($id)
    {
        $slide = Slide::find($id);

        if($slide->delete()){

            Flash::message('Banner eliminado correctamente.');
        }

        return redirect(route('slides.index'));
    }
}
