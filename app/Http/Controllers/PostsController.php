<?php

namespace Doz\Http\Controllers;

use Doz\PostCategory;
use Doz\Post;
use Doz\Http\Requests\PostCategoryRequest;
use Doz\Http\Requests\PostRequest;
use Laracasts\Flash\Flash;

class PostsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $posts = Post::paginate(5);

        return view('app.posts.index', compact('posts'));
    }

    public function create()
    {
        return view('app.posts.create');
    }

    public function store(PostRequest $request)
    {
        $post = new Post();

        $post->fill($request->all());

        if($post->save()) {

            Flash::success('Post creado correctamente');
        }

        return redirect(route('posts.index'));
    }

    public function edit($id)
    {
        $post = Post::find($id);

        return view('app.posts.edit', compact('post'));
    }

    public function update(PostRequest $request, $id)
    {
        $post = Post::find($id);

        $post->fill($request->all());

        if($post->save()) {

            Flash::success('Post actualizado correctamente.');
        }

        return redirect(route('posts.index'));
    }

    public function destroy($id)
    {
        $post = Post::find($id);

        $post->delete();

        return redirect(route('posts.index'));
    }

    public function indexCategory()
    {
        $categories = PostCategory::paginate(5);

        return view('app.posts.categories.index', compact('categories'));
    }

    public function createCategory()
    {
        $categories = PostCategory::all();

        return view('app.posts.categories.create', compact('categories'));
    }

    public function storeCategory(PostCategoryRequest $request)
    {
        $category = new PostCategory();

        $category->category = $request->get('category');

        $category->subcategory = ($request->get('subcategory') == '') ? 0 : $request->get('subcategory');

        if($category->save()) {

            Flash::message('Categoria creada correctamente.');
        }

        return redirect(route('posts.category.index'));
    }

    public function editCategory($id)
    {
        $category = PostCategory::find($id);

        return view('app.posts.categories.edit', compact('category'));
    }

    public function updateCategory(PostCategoryRequest $request, $id)
    {
        $category = PostCategory::find($id);

        $category->fill($request->all());

        if($category->save()) {

            Flash::message('Categoria actualizada correctamente.');
        }

        return redirect(route('posts.category.index'));
    }

    public function deleteCategory($id)
    {
        $category = PostCategory::find($id);

        $category->delete();

        return redirect(route('posts.category.index'));
    }
}
