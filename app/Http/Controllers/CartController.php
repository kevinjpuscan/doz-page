<?php

namespace Doz\Http\Controllers;

use Doz\Product;
use Doz\ProductCategory;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;

use Doz\Http\Requests;
use Laracasts\Flash\Flash;

class CartController extends Controller
{

    public function index()
    {
        return view('cart');
    }

    public function store(Request $request)
    {
        if(Cart::search(['id' => $request->id])) {

            Flash::info('Este producto ya ha sido agregado al carrito.');

            return redirect()->back();
        }

        Cart::associate('Doz\Product')->add([
            'id' => $request->id,
            'name' => $request->name,
            'qty' => 1,
            'price' => $request->price
        ]);

        Flash::success('Producto agregado al carrito.');

        return redirect()->back();
    }

    public function checkout()
    {
        return view('checkout');
    }

    public function shop()
    {
        $products = Product::paginate(9);

        $categories = ProductCategory::where('subcategory', 0)->with('children')->get();

        $brands = ProductCategory::where('subcategory', '<>', 0)->get();

        return view('shop', compact('products', 'categories', 'brands'));
    }
}
