<?php

namespace Doz\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SlideRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $slides = $this->slides;

        switch ($this->method())
        {
            case 'GET':
                return [];
            case 'POST':
                return [
                    'heading_first' => 'required',
                    'heading_second' => 'required',
                    'paragraph' => 'required',
                    'text_button' => 'required',
                    'url' => 'required'
                ];
            case 'PATCH':
                return [
                    'heading_first' => 'required',
                    'heading_second' => 'required',
                    'paragraph' => 'required',
                    'text_button' => 'required',
                    'url' => 'required'
                ];
            case 'DELETE':
                return [];
            default:
                break;
        }
    }
}
