<?php

namespace Doz\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method())
        {
            case "GET":
                return [];
                break;
            case "POST":
                return [
                    'category' => 'required',
                    'subcategory' => 'required',
                    'slug' => 'required'
                ];
                break;
            case "PATCH":
                return [
                    'category' => 'required',
                    'subcategory' => 'required'
                ];
                break;
            case "DELETE":
                return [];
            default:
                break;
        }
    }
}
