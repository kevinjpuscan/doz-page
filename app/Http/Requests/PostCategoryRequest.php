<?php

namespace Doz\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $categories = $this->categories;

        switch ($this->method())
        {
            case 'GET':
                return [];
            case 'POST':
                return [
                    'category' => 'required'
                ];
            case 'PATCH':
                return [
                    'category' => 'required'
                ];
            case 'DELETE':
                return [];
            default:
                break;
        }
    }
}
