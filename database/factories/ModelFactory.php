<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(Doz\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(Doz\Product::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->domainName,
        'photo' => 'bower_components/eshopper/images/home/product1.jpg',
        'description' => $faker->text,
        'price' => $faker->numberBetween(1, 100),
        'slug' => $faker->domainName,
        'categories_id' => $faker->numberBetween(1, 3),
        'users_id' => $faker->numberBetween(1, 3),
    ];
});
