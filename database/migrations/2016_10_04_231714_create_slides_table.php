<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlidesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slides', function(Blueprint $table){
            $table->increments('id');
            $table->string('heading_first');
            $table->string('heading_second');
            $table->string('paragraph');
            $table->string('text_button');
            $table->string('url');
            $table->string('photo')->default('bower_components/eshopper/images/banner-1.png');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('slides');
    }
}
