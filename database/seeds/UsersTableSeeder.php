<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Doz\User::class)->create([
            'name' => 'Diana Marcela Tellez Ebratt',
            'email' => 'ventas@doz.com.co',
            'password' => bcrypt('123'),
            'role' => 'super-admin'
        ]);
    }
}
