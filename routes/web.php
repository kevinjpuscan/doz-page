<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Auth::routes();
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('contact', 'WebController@contact')->name('contact');
Route::get('blog/{slug}', 'BlogController@show')->name('blog.slug');
Route::get('checkout', 'CartController@checkout')->name('checkout');
Route::post('agregar-carrito', 'CartController@store')->name('add.cart');
Route::get('shop', 'CartController@shop')->name('shop');
Route::resource('app', 'HomeController');
Route::resource('/', 'WebController');
Route::resource('cart', 'CartController');
Route::resource('blog', 'BlogController');
Route::get('detalles/{slug}', 'WebController@show')->name('product.detail');

Route::resource('posts', 'PostsController');
Route::get('posts/category/index', 'PostsController@indexCategory')->name('posts.category.index');
Route::get('posts/category/create', 'PostsController@createCategory')->name('posts.category.create');
Route::post('posts/category/store', 'PostsController@storeCategory')->name('posts.category.store');
Route::get('posts/category/{id}/edit', 'PostsController@editCategory')->name('posts.category.edit');
Route::patch('posts/category/{id}/update', 'PostsController@updateCategory')->name('posts.category.update');
Route::delete('posts/category/{id}/delete', 'PostsController@deleteCategory')->name('posts.category.delete');

Route::resource('pages', 'PagesController');
Route::resource('products', 'ProductsController');
Route::resource('categories', 'ProductCategoriesController');

Route::resource('slides', 'SlidesController');
Route::resource('stores', 'StoresController');
Route::resource('users', 'UsersController');

Route::resource('messages', 'MessagesController');