<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Las contraseñas deben tener por lo menos 6 caracteres y tener confirmacion.',
    'reset' => 'Tu contraseña ha sido reseteada!',
    'sent' => 'Hemos enviado un enlace a tu corre para resetear tu contraseña!',
    'token' => 'Este token para resetear la contraseña no es valido.',
    'user' => "No encontramos un usuarios con esa direccion de correo electronico.",

];
