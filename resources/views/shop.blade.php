@extends('layouts.web')

@section('title', 'Shop')

@section('section')

    <section id="advertisement">
        <div class="container">
            <img src="{{ asset('bower_components/eshopper/images/shop/advertisement.jpg') }}" alt="" />
        </div>
    </section>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div class="left-sidebar">
                        <h2>@lang('web.category')</h2>

                        @include('layouts.partials.web.category', [
                        'categories' => $categories,
                        'brands' => $brands
                        ])

                        @include('layouts.partials.web.brands')

                        @include('layouts.partials.web.price-range')

                        @include('layouts.partials.web.promo')

                    </div>
                </div>

                <div class="col-sm-9 padding-right">
                    <div class="features_items"><!--features_items-->
                        <h2 class="title text-center">@lang('web.featured-items')</h2>
                        @include('flash::message')
                        @foreach($products as $product)
                        <div class="col-sm-4">
                            <div class="product-image-wrapper">
                                <div class="single-products">
                                    <div class="productinfo text-center">
                                        <img src="{{ $product->photo }}" alt="" />
                                        <h2>${{ $product->price }}</h2>
                                        <p>{{ $product->name }}</p>
                                        <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>@lang('web.add-to-cart')</a>
                                    </div>
                                    <div class="product-overlay">
                                        <div class="overlay-content">
                                            <h2>${{ $product->price }}</h2>
                                            <p>{{ $product->name }}</p>
                                            {!! Form::open(['route' => 'add.cart', 'method' => 'POST']) !!}
                                            {!! Form::hidden('id', $product->id) !!}
                                            {!! Form::hidden('name', $product->name) !!}
                                            {!! Form::hidden('price', $product->price) !!}
                                            <button type="submit" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>@lang('web.add-to-cart')</button>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="choose">
                                    <ul class="nav nav-pills nav-justified">
                                        <li><a href=""><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
                                        <li><a href=""><i class="fa fa-plus-square"></i>Add to compare</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        @endforeach

                        <div class="row">
                            <div class="col-sm-12">
                                {!! $products->render() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @endsection