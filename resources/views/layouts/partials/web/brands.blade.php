<div class="brands_products"><!--brands_products-->
    <h2>@lang('web.brands')</h2>
    <div class="brands-name">
        <ul class="nav nav-pills nav-stacked">
            @foreach($brands as $brand)
                <li><a href="#"> <span class="pull-right">(*)</span>{{ $brand->category }}</a></li>
                @endforeach
        </ul>
    </div>
</div><!--/brands_products-->