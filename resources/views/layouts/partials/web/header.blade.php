<header id="header"><!--header-->
    <div class="header_top"><!--header_top-->
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="contactinfo">
                        <ul class="nav nav-pills">
                            <li><a href="#"><i class="fa fa-phone"></i> 310 7516036</a></li>
                            <li><a href="#"><i class="fa fa-envelope"></i> ventas@doz.com.co</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="social-icons pull-right">
                        <ul class="nav navbar-nav">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <!--<li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>-->
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/header_top-->

    <div class="header-middle"><!--header-middle-->
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="logo pull-left">
                        <a href="{{ route('index') }}">
                            <img src="{{ asset('bower_components/eshopper/images/home/logo.png') }}" alt="" />
                        </a>
                    </div>
                    <!--<div class="btn-group pull-right">
                        <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
                                USA
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="#">Canada</a></li>
                                <li><a href="#">UK</a></li>
                            </ul>
                        </div>

                        <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
                                DOLLAR
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="#">Canadian Dollar</a></li>
                                <li><a href="#">Pound</a></li>
                            </ul>
                        </div>
                    </div>-->
                </div>
                <div class="col-sm-8">
                    <div class="shop-menu pull-right">
                        <ul class="nav navbar-nav">
                            @if(Auth::check())
                            <li><a href="{{ route('app.index') }}"><i class="fa fa-user"></i> @lang('web.account')</a></li>
                                @else
                                @endif
                            <li><a href="#"><i class="fa fa-star"></i> @lang('web.wishlist')</a></li>
                            <li><a href="{{ url('checkout') }}"><i class="fa fa-crosshairs"></i> @lang('web.checkout')</a></li>
                            <li><a href="{{ url('cart') }}"><i class="fa fa-shopping-cart"></i> @lang('web.cart') ({{ Cart::instance('main')->count(false) }})</a></li>
                            @if(Auth::check())
                                <li><a href="{{ route('logout') }}">Cerrar sesion</a></li>
                                @else
                                <li><a href="{{ url('login') }}"><i class="fa fa-lock"></i> @lang('web.login')</a></li>
                                @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/header-middle-->

    <div class="header-bottom"><!--header-bottom-->
        <div class="container">
            <div class="row">
                <div class="col-sm-9">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="mainmenu pull-left">
                        <ul class="nav navbar-nav collapse navbar-collapse">
                            <li><a href="{{ route('index') }}" class="active">@lang('web.home')</a></li>
                            <li class="dropdown">
                                <a href="{{ route('shop') }}">@lang('web.shop')<!--<i class="fa fa-angle-down"></i>-->
                                </a>
                                <!--<ul role="menu" class="sub-menu">
                                    <li><a href="{{ route('shop') }}">@lang('web.shop')</a></li>
                                    <li><a href="{{ route('checkout') }}">@lang('web.checkout')</a></li>
                                    <li><a href="{{ route('cart.index') }}">@lang('web.cart')</a></li>
                                </ul>-->
                            </li>
                            <li class="dropdown"><a href="#">@lang('web.blog')<i class="fa fa-angle-down"></i></a>
                                <ul role="menu" class="sub-menu">
                                    <li><a href="{{ url('blog') }}">@lang('web.blog')</a></li>
                                    <li><a href="#">@lang('web.blog-post')</a></li>
                                </ul>
                            </li>
                            <li><a href="{{ url('contact') }}">@lang('web.contact')</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="search_box pull-right">
                        <input type="text" placeholder="@lang('web.search')"/>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/header-bottom-->
</header><!--/header-->