<div class="panel-group category-products" id="accordian" role="tablist" aria-multiselectable="true"><!--category-productsr-->
    @foreach($categories as $category)
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordian" href="#{{ $category->slug }}">
                        <span class="badge pull-right">
                            <i class="fa fa-plus"></i>
                        </span>
                        {{ $category->category }}
                    </a>
                </h4>
            </div>

            <div id="{{ $category->slug }}" class="panel-collapse collapse">
                <div class="panel-body">
                    <ul>
                        @foreach($category->children as $child)
                            <li>
                                <a href="#">{{ $child->category }}</a>
                            </li>
                            @endforeach
                    </ul>
                </div>
            </div>
        </div>
        @endforeach
</div><!--/category-products-->