<div class="panel-group category-products" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingZero">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseZero" aria-expanded="true" aria-controls="collapseZero">
                    <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                    Escritorio
                </a>
            </h4>
        </div>

        <div id="collapseZero" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingZero">
            <div class="panel-body">
                <ul>
                    <li>
                        <a href="{{ route('app.index') }}">Escritorio</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                    Banner
                </a>
            </h4>
        </div>

        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
                <ul>
                    <li>
                        <a href="{{ route('slides.create') }}">Crear banner</a>
                    </li>
                    <li>
                        <a href="{{ route('slides.index') }}">Lista de banner</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingTwo">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                    Blog
                </a>
            </h4>
        </div>
        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
            <div class="panel-body">
                <ul>
                    <li>
                        <a href="{{ route('posts.category.create') }}">Crear categoria</a>
                    </li>

                    <li>
                        <a href="{{ route('posts.category.index') }}">Listar categorias</a>
                    </li>

                    <li>
                        <a href="{{ route('posts.create') }}">Crear post</a>
                    </li>

                    <li>
                        <a href="{{ route('posts.index') }}">Listar posts</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingThree">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                    <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                    Productos
                </a>
            </h4>
        </div>
        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
            <div class="panel-body">
                <ul>
                    <li>
                        <a href="{{ route('categories.create') }}">Crear categoria</a>
                    </li>

                    <li>
                        <a href="{{ route('categories.index') }}">Listar categorias</a>
                    </li>

                    <li>
                        <a href="{{ route('products.create') }}">Crear producto</a>
                    </li>

                    <li>
                        <a href="{{ route('products.index') }}">Listar productos</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingFour">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                    <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                    Mensajes
                </a>
            </h4>
        </div>
        <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
            <div class="panel-body">
                <ul>
                    <li>
                        <a href="#">Listar mensajes</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>