<!DOCTYPE html>
<html ng-app="Doz">
<head>
    <title>Doz | @yield('title')</title>
    <link rel="stylesheet" href="{{ asset('bower_components/eshopper/css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('bower_components/eshopper/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('bower_components/eshopper/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('bower_components/eshopper/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('bower_components/eshopper/css/prettyPhoto.css') }}">
    <link rel="stylesheet" href="{{ asset('bower_components/eshopper/css/price-range.css') }}">
    <link rel="stylesheet" href="{{ asset('bower_components/eshopper/css/responsive.css') }}">
    <script src="{{ asset('bower_components/eshopper/js/html5shiv.js') }}"></script>
</head>
<body id="app" class="hold-transition skin-blue sidebar-mini">
    @include('layouts.partials.web.header')
    <section class="container">
        @yield('section')
    </section>
    @include('layouts.partials.web.footer')
    <script src="{{ asset('bower_components/eshopper/js/jquery.js') }}"></script>
    <script src="{{ asset('bower_components/angular/angular.min.js') }}"></script>
    <script src="{{ asset('bower_components/eshopper/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('bower_components/eshopper/js/jquery.scrollUp.min.js') }}"></script>
    <script src="{{ asset('bower_components/eshopper/js/price-range.js') }}"></script>
    <script src="{{ asset('bower_components/eshopper/js/jquery.prettyPhoto.js') }}"></script>
    <script src="{{ asset('bower_components/eshopper/js/main.js') }}"></script>
    <script>

        var app = angular.module("Doz", []);

        app.filter("slugify", function() {
            return function(input) {
                input = input || '';

                return input.replace(/ /g, '-').toLowerCase();
            }
        });

        $(document).ready(function(){

            var app = window.location.pathname;

            switch (app) {
                case "/app":
                    $("#collapseZero").addClass("in");
                    break;
                case "/slides":
                case "/slides/create":
                    $("#collapseOne").addClass("in");
                    break;
                case "/posts/category/index":
                case "/posts/category/create":
                case "/posts/create":
                case "/posts":
                    $("#collapseTwo").addClass("in");
                    break;
                case "/categories/create":
                case "/categories":
                case "/products/create":
                case "/products":
                    $("#collapseThree").addClass("in");
                    break;
                default:
                    break;
            }
        });
    </script>
</body>
</html>