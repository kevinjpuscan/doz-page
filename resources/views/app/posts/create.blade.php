@extends('layouts.web')

@section('title', 'Crear slides')

@section('section')

    <div class="row">
        <div class="col-md-3">
            @include('layouts.partials.app.menu')
        </div>
        <div class="col-md-9">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="title text-center">Crear <strong>Post</strong></h2>
                </div>
            </div>
            <div class="contact-form">
                {{ Form::open(['route' => 'posts.store', 'method' => 'POST', 'class' => 'row']) }}
                <div class="form-group col-md-6">
                    {!! Form::text('titulo', null, ['class' => 'form-control', 'placeholder' => 'Titulo']) !!}
                </div>

                <div class="form-group col-md-6">
                    {!! Form::text('slug', null, ['class' => 'form-control', 'placeholder' => 'Slug']) !!}
                </div>

                <div class="form-group col-md-12">
                    {!! Form::textarea('cuerpo', null, ['class' => 'form-control', 'placeholder' => 'Cuerpo']) !!}
                </div>

                <div class="form-group col-md-12">
                    {!! Form::submit('Crear', ['class' => 'btn btn-primary btn-block']) !!}
                </div>

                {{ Form::close() }}
            </div>
        </div>
    </div>

@endsection