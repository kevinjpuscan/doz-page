@extends('layouts.web')

@section('title', 'Posts')

@section('section')


    <div class="row">
        <div class="col-md-3">
            @include('layouts.partials.app.menu')
        </div>

        <div class="col-md-9">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="title text-center">Listar <strong>Posts</strong></h2>
                </div>
            </div>

            <div class="row">
                <table class="table table-condensed">
                    <thead>
                    <tr class="cart_menu">
                        <th>Entradas</th>
                        <th>Descripcion</th>
                        <th>Categoria</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($posts as $post)
                        <tr>
                            <td class="cart_product">
                                <a href=""><img src="#" width="150" alt=""></a>
                            </td>

                            <td class="cart_description">
                                <h4><a href="">Titulo 1: </a></h4>
                                <p>Titulo 2: </p>
                            </td>

                            <td class="cart_price">
                                <p></p>
                            </td>

                            <td class="cart_quantity">
                                <div class="cart_quantity_button">

                                </div>
                            </td>

                            <td class="cart_delete">
                                {{--{!! Form::open(['route' => ['slides.destroy', 'id' => $slide->id ], 'method' => 'DELETE']) !!}
                                <button class="btn btn-primary" type="submit"><i class="fa fa-times"></i></button>
                                {!! Form::close() !!}
                                <a href="{{ route('slides.edit', $slide->id) }}" class="btn btn-primary"><i class="fa fa-pencil"></i></>--}}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection