@extends('layouts.web')

@section('title', 'Listar Categorias')

@section('section')

    <div class="row">
        <div class="col-md-3">
            @include('layouts.partials.app.menu')
        </div>

        <div class="col-md-9">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="title text-center">Listar <strong>Categorias</strong></h2>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    @if($categories->isEmpty())
                        <div class="alert alert-warning">
                            No hay categorias creadas
                        </div>
                    @else
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Categoria</th>
                                    <th>Categoria padre</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($categories as $category)
                                    <tr>
                                        <td>{{ $category->category }}</td>
                                        <td>{{ $category->subcategory }}</td>
                                        <td>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <a class="btn btn-primary btn-block" href="{{ route('posts.category.edit', $category->id) }}"><i class="fa fa-pencil"></i></a>
                                                </div>
                                                <div class="col-md-6">
                                                    {!! Form::open(['route' => ['posts.category.delete', $category->id], 'method' => 'DELETE']) !!}
                                                    <button class="btn btn-primary btn-block" type="submit"><i class="fa fa-times"></i></button>
                                                    {!! Form::close() !!}
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    @endsection