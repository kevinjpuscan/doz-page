@extends('layouts.web')

@section('title', 'Crear slides')

@section('section')

    <div class="row">
        <div class="col-md-3">
            @include('layouts.partials.app.menu')
        </div>
        <div class="col-md-9">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="title text-center">Crear <strong>Categoria</strong></h2>
                </div>
            </div>
            <div>
                @if($errors->any())
                    @foreach($errors->all() as $error)
                        <div class="alert alert-danger">
                            {{ $error }}
                        </div>
                    @endforeach
                @endif
            </div>
            @include('flash::message')
            <div class="contact-form">
                {{ Form::open(['route' => 'posts.category.store', 'method' => 'POST', 'class' => 'row']) }}
                <div class="form-group col-md-6">
                    {!! Form::text('category', null, ['class' => 'form-control', 'placeholder' => 'Categoria']) !!}
                </div>

                <div class="form-group col-md-6">
                    {!! Form::text('subcategory', null, ['class' => 'form-control', 'placeholder' => 'Subcategoria']) !!}
                </div>

                <div class="form-group col-md-12">
                    {!! Form::submit('Crear', ['class' => 'btn btn-primary btn-block']) !!}
                </div>

                {{ Form::close() }}
            </div>
        </div>
    </div>

@endsection