@extends('layouts.web')

@section('title', 'Editar slide')

@section('section')

    <div class="row">
        <div class="col-md-4">
            @include('layouts.partials.app.menu')
        </div>

        <div class="col-md-8">
            <div class="contact-form">
                <h2 class="title text-center">Editar banner</h2>
                <div class="status alert alert-success" style="display: none"></div>
                {!! Form::model($slide, ['route' => ['slides.update', $slide], 'method' => 'PATCH', 'files' => true, 'class' => 'contact-form row']) !!}
                <div class="form-group col-md-6">
                    {!! Form::text('heading_first', null, ['class' => 'form-control', 'placeholder' => 'Primer titulo']) !!}
                </div>

                <div class="form-group col-md-6">
                    {!! Form::text('heading_second', null, ['class' => 'form-control', 'plcaholder' => 'Segundo titulo']) !!}
                </div>

                <div class="form-group col-md-6">
                    {!! Form::text('paragraph', null, ['class' => 'form-control', 'plcaholder' => 'Parrafo']) !!}
                </div>

                <div class="form-group col-md-6">
                    {!! Form::text('text_button', null, ['class' => 'form-control', 'placeholder' => 'Texto boton']) !!}
                </div>

                <div class="form-group col-md-12">
                    {!! Form::text('url', null, ['class' => 'form-control', 'placeholder' => 'Enlace']) !!}
                </div>

                <div class="form-group col-md-12">
                    {!! Form::file('photo', ['class' => 'form-control']) !!}
                </div>

                <div class="form-group col-md-12">
                    <img class="img-responsive" src="{{ $slide->photo }}" alt="">
                </div>

                <div class="form-group col-md-12">
                    {!! Form::submit('Guardar', ['class' => 'btn btn-primary btn-block']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    @endsection