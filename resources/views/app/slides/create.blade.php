@extends('layouts.web')

@section('title', 'Crear slides')

@section('section')

    <div class="row">
        <div class="col-md-3">
            @include('layouts.partials.app.menu')
        </div>
        <div class="col-md-9">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="title text-center">Crear <strong>Banner</strong></h2>
                </div>
            </div>

            @include('flash::message')

            <div>
                @if($errors->any())
                    @foreach($errors->all() as $error)
                        <div class="alert alert-danger">
                            {{ $error }}
                        </div>
                    @endforeach
                @endif
            </div>

            <div class="contact-form">
                {{ Form::open(['route' => 'slides.store', 'method' => 'POST', 'files' => true, 'class' => 'row']) }}
                <div class="form-group col-md-6">
                    {!! Form::text('heading_first', null, ['class' => 'form-control', 'placeholder' => 'Primer titulo']) !!}
                </div>

                <div class="form-group col-md-6">
                    {!! Form::text('heading_second', null, ['class' => 'form-control', 'placeholder' => 'Segundo titulo']) !!}
                </div>

                <div class="form-group col-md-6">
                    {!! Form::text('paragraph', null, ['class' => 'form-control', 'placeholder' => 'Parrafo']) !!}
                </div>

                <div class="form-group col-md-6">
                    {!! Form::text('text_button', null, ['class' => 'form-control', 'placeholder' => 'Texto boton']) !!}
                </div>

                <div class="form-group col-md-6">
                    {!! Form::text('url', null, ['class' => 'form-control', 'placeholder' => 'Enlace']) !!}
                </div>

                <div class="form-group col-md-6">
                    {!! Form::file('photo', ['class' => 'form-control', 'placeholder' => 'Foto']) !!}
                </div>

                <div class="form-group col-md-12">
                    {!! Form::submit('Crear', ['class' => 'btn btn-primary btn-block']) !!}
                </div>

                {{ Form::close() }}
            </div>
        </div>
    </div>

    @endsection