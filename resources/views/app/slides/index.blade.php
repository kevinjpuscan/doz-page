@extends('layouts.web')

@section('title', 'Slides')

@section('section')

    <div class="row">
        <div class="col-md-3">
            @include('layouts.partials.app.menu')
        </div>

        <div class="col-md-9">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="title text-center">Listar <strong>Banner</strong></h2>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    @if($slides->isEmpty())
                        <div class="alert alert-warning">
                            No hay banners creados
                        </div>
                        @else
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Foto</th>
                                    <th>Primer titulo</th>
                                    <th>Segundo titulo</th>
                                    <th>Parrafo</th>
                                    <th>Texto del boton</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($slides as $slide)
                                <tr>
                                    <td>
                                        <img width="100" class="img-responsive" src="{{ asset($slide->photo) }}" alt="">
                                    </td>
                                    <td>{{ $slide->heading_first }}</td>
                                    <td>{{ $slide->heading_second }}</td>
                                    <td>{{ $slide->paragraph }}</td>
                                    <td><a href="{{ $slide->url }}" class="btn btn-primary">{{ $slide->text_button }}</a></td>
                                    <td>
                                        <a class="btn btn-primary btn-block" href="{{ route('slides.edit', $slide->id) }}">Edit</a>
                                        {!! Form::open(['route' => ['slides.destroy', $slide->id], 'method' => 'DELETE']) !!}
                                        {!! Form::submit('Delete', ['class' => 'btn btn-primary btn-block']) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        @endif
                </div>
            </div>
        </div>
    </div>

@endsection