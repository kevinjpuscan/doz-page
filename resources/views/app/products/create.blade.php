@extends('layouts.web')

@section('title', 'Product Create')

@section('section')

    <div class="row">
        <div class="col-md-3">
            @include('layouts.partials.app.menu')
        </div>

        <div class="col-md-9">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="title text-center">Crear <strong>Producto</strong></h2>
                </div>
            </div>

            @include('app.errors.errors')

            @include('flash::message')

            <div class="row">
                <div class="col-md-12">
                    {!! Form::open(['route' => 'products.store', 'method' => 'POST', 'class' => 'row', 'files' => true]) !!}

                    @include('app.products.fields')

                    @include('app.products.buttons')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
