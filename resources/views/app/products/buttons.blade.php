<div class="form-group col-md-6">
    {!! Form::submit('Crear', ['class' => 'btn btn-primary btn-block']) !!}
</div>

<div class="form-group col-md-6">
    <a class="btn btn-primary btn-block" href="{{ route('products.index') }}">Volver</a>
</div>