@extends('layouts.web')

@section('title', 'Products')

@section('section')

    <div class="row">
        <div class="col-md-3">
            @include('layouts.partials.app.menu')
        </div>

        <div class="col-md-9">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="title text-center">Listar <strong>Productos</strong></h2>
                </div>
            </div>

            @include('app.errors.errors')

            @include('flash::message')

            @foreach($products as $product)
            <div class="row">
                <div class="col-md-2">
                    <img class="img-responsive" src="{{ $product->photo }}" alt="">
                </div>
                <div class="col-md-10">
                    <h3>{{ $product->name }}</h3>
                    <p>{{ $product->description }}</p>
                    <div class="row">
                        <div class="col-md-8">
                            <h4>{{ $product->price }}</h4>
                        </div>
                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-md-6">
                                    <a class="btn btn-block btn-primary" href="{{ route('products.edit', $product) }}"><i class="fa fa-pencil"></i></a>
                                </div>
                                <div class="col-md-6">
                                    {!! Form::open(['route' => ['products.destroy', $product], 'method' => 'DELETE']) !!}
                                    <button type="submit" class="btn btn-block btn-primary"><i class="fa fa-times"></i></button>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach

            <div class="row">
                <div class="col-md-12">
                    {!! $products->render() !!}
                </div>
            </div>
        </div>
    </div>

@endsection