@extends('layouts.web')

@section('title', 'Product Edit')

@section('section')

    <div class="row">
        <div class="col-md-3">
            @include('layouts.partials.app.menu')
        </div>

        <div class="col-md-9">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="title text-center">Editar <strong>Producto</strong></h2>
                </div>
            </div>

            @include('app.errors.errors')

            @include('flash::message')

            <div class="row">
                <div class="col-md-12">
                    {!! Form::model($product, ['route' => ['products.update', 'id' => $product], 'method' => 'PATCH', 'class' => 'row', 'files' => true]) !!}

                    @include('app.products.fields')

                    @include('app.products.buttons')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
