@extends('layouts.web')

@section('title', 'Create Category Product')

@section('section')

    <div class="row">
        <div class="col-md-3">
            @include('layouts.partials.app.menu')
        </div>
        <div class="col-md-9">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="title text-center">Crear <strong>Categoria</strong></h2>
                </div>
            </div>

            @include('app.errors.errors')

            @include('flash::message')

            <div class="contact-form">
                {{ Form::open(['route' => 'categories.store', 'method' => 'POST', 'class' => 'row']) }}

                @include('app.products.categories.fields')

                @include('app.products.categories.buttons')

                {{ Form::close() }}
            </div>
        </div>
    </div>

@endsection