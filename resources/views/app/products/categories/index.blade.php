@extends('layouts.web')

@section('title', 'Category Products')

@section('section')

    <div class="row">
        <div class="col-md-3">
            @include('layouts.partials.app.menu')
        </div>
        <div class="col-md-9">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="title text-center">Listar <strong>Categorias</strong></h2>
                </div>
            </div>

            @include('app.errors.errors')

            @include('flash::message')

            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Categoria</th>
                                <th>Subcategoria</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($categories as $category)
                                <tr>
                                    <td>{{ $category->category }}</td>
                                    <td>@if($category->subcategory == 0) Padre @else {{ $category->parent['category'] }} @endif</td>
                                    <td>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <a href="{{ route('categories.edit', $category) }}" class="btn btn-primary btn-block"><i class="fa fa-pencil"></i></a>
                                            </div>

                                            <div class="col-md-6">
                                                {!! Form::open(['route' => ['categories.destroy', $category], 'method' => 'DELETE']) !!}
                                                <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-times"></i></button>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection