<div class="form-group col-md-6">
    {!! Form::text('category', null, ['class' => 'form-control', 'placeholder' => 'Categoria', 'ng-model' => 'category']) !!}
</div>

<div class="form-group col-md-6">
    {!! Form::select('subcategory', $categories, null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-md-12">
    {!! Form::hidden('slug', null, ['class' => 'form-control', 'ng-value' => 'category | slugify']) !!}
</div>

