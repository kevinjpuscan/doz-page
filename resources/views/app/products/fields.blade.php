<div class="form-group col-md-6">
    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nombre producto', 'ng-model' => 'slug']) !!}
</div>

<div class="form-group col-md-6">
    {!! Form::text('description', null, ['class' => 'form-control', 'placeholder' => 'Descripcion']) !!}
</div>

<div class="form-group col-md-6">
    {!! Form::text('price', null, ['class' => 'form-control', 'placeholder' => 'Precio']) !!}
</div>

<div class="form-group col-md-6">
    {!! Form::select('categories_id', $categories, null, ['class' => 'form-control', 'placeholder' => 'Selecciona una categoria']) !!}
</div>

<div class="form-group col-md-6">
    {!! Form::hidden('users_id', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-md-12">
    {!! Form::file('photo', ['class' => 'form-control']) !!}
</div>

<div class="form-group col-md-12">
    {!! Form::hidden('slug', null, ['class' => 'form-control', 'placeholder' => 'Slug', 'ng-value' => 'slug | slugify']) !!}
</div>