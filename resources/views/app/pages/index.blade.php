@extends('layouts.web')

@section('title', 'Pages')

@section('section')

    <section class="container">
        <div class="table-responsive cart_info">
            <table class="table table-condensed">
                <thead>
                <tr class="cart_menu">
                    <td class="image">Imagen</td>
                    <td class="description"></td>
                    <td class="price">Parrafo</td>
                    <td class="quantity">Texto boton</td>
                    <td class="total">Total</td>
                </tr>
                </thead>
                <tbody>
                @foreach($pages as $page)
                    <tr>
                        <td class="cart_product">
                            <a href=""><img src="#" width="150" alt=""></a>
                        </td>

                        <td class="cart_description">
                            <h4><a href="">Titulo 1: </a></h4>
                            <p>Titulo 2: </p>
                        </td>

                        <td class="cart_price">
                            <p></p>
                        </td>

                        <td class="cart_quantity">
                            <div class="cart_quantity_button">

                            </div>
                        </td>

                        <td class="cart_delete">
                            {{--!! Form::open(['route' => ['slides.destroy', 'id' => $slide->id ], 'method' => 'DELETE']) !!}
                            <button class="btn btn-primary" type="submit"><i class="fa fa-times"></i></button>
                            {!! Form::close() !!}
                            <a href="{{ route('slides.edit', $slide->id) }}" class="btn btn-primary"><i class="fa fa-pencil"></i></>--}}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </section>
    @endsection