@extends('layouts.web')

@section('title', 'Ingresar')

@section('section')
    <section id="form"><!--form-->
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 col-sm-offset-1">
                    <div class="login-form"><!--login form-->
                        <h2>Login to your account</h2>

                        {!! Form::open(['url' => 'login', 'method' => 'POST']) !!}
                            {!! Form::text('email', null, ['placeholder' => 'Email']) !!}
                            {!! Form::password('password', ['placeholder' => 'Password']) !!}
                            <span>
								<input type="checkbox" class="checkbox">
								Keep me signed in
							</span>
                            <button type="submit" class="btn btn-default">Login</button>
                        {!! Form::close() !!}
                    </div><!--/login form-->
                </div>
                <div class="col-sm-1">
                    <h2 class="or">OR</h2>
                </div>
                <div class="col-sm-4">
                    <div class="signup-form"><!--sign up form-->
                        <h2>New User Signup!</h2>
                        {!! Form::open(['url' => 'register', 'method' => 'POST']) !!}
                        {!! Form::text('name', null, ['placeholder' => 'Name']) !!}
                        {!! Form::email('email', null, ['placeholder' => 'Email']) !!}
                        {!! Form::password('password', ['placeholder' => 'Password']) !!}
                        {!! Form::password('password_confirmation', ['placeholder' => 'Password Confirmation']) !!}
                            <button type="submit" class="btn btn-default">Signup</button>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section><!--/form-->
@endsection
